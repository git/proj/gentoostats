
import json
try:
    import http.client as httplib
except ImportError:
    import httplib

# json headers for gentoostats-cli
headers = {'Accept': 'application/json'}

def GET(server, port, url, headers, https=True):
    """
    Get url from server using headers 
    """
    if https:
        conn = httplib.HTTPSConnection(server, port)
    else:
        conn = httplib.HTTPConnection(server, port)
    try:
        conn.request('GET', url=url, headers=headers)
        data = conn.getresponse().read().decode("utf-8")
    except httplib.HTTPException:
        return None
    finally:
        if conn:
            conn.close()
    return data

def deserialize(object):
    """
    Decode json object
    """
    try:
        decoded = json.JSONDecoder().decode(object)
    except (ValueError, TypeError):
        return None
    return decoded
